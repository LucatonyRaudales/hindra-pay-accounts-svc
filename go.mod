module github.com/LucatonyRaudales/hindra-pay-accounts-svc

go 1.18

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.4.0
	gorm.io/gorm v1.23.6
)

require github.com/felixge/httpsnoop v1.0.1 // indirect

require (
	github.com/badoux/checkmail v1.2.1 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
