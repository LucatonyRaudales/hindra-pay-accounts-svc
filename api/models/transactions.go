package models

import (
	"time"
	"strings"
	"html"
	"errors"
  "github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

type Status string

// I created the enum type in the db befor using:
// CREATE TYPE transaction_status AS ENUM ('pending', 'success', 'failed');

const (
	Pending Status = "pending"
	Success Status = "success"
	Failed 	Status = "failed"
)

type TransactionSuccessResponse struct {
	Message string `json:"message"`
	TransactionID string `json:"transactionId"`
	NewBalance float64 `json:"newBalance"`
}

type SwapSuccessResponse struct {
	Message string `json:"message"`
	TransactionID string `json:"transactionId"`
	FromName string `json:"fromName"`
	FromNewBalance float64 `json:"fromNewBalance"`
	ToName string `json:"toName"`
	ToNewBalance float64 `json:"toNewBalance"`

}
type TransactionFailedResponse struct {
	Message string `json:"message"`
	Description string `json:"description"`
}
type Transaction struct {
	ID string `gorm:"type:uuid;primary_key" json:"id"`
	AccountFrom string `gorm:"type:uuid;not null;" json:"from"`
	//AccountFrom Account `gorm:"foreignKey:From" json:"accountFrom"`
	AccountTo string `gorm:"type:uuid;not null;" json:"to"`
	//AccountTo Account `gorm:"foreignKey:To" json:"accountTo"`
	Amount float64 `gorm:"type:decimal(10,2);not null;" json:"amount"`
	Status string `gorm:"type:transaction_status" json:"status"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"createdAt"`
}

func (data *Transaction) Prepare() {
	data.ID = uuid.NewV4().String()
	data.AccountFrom = html.EscapeString(strings.TrimSpace(data.AccountFrom))
	data.AccountTo = html.EscapeString(strings.TrimSpace(data.AccountTo))
	data.CreatedAt = time.Now()
	data.Status =  html.EscapeString(strings.TrimSpace("pending"))
}

func (data *Transaction) Validate() error { 
	if data.AccountFrom == "" {
		return errors.New("from (account id from the amount is sending) field is required")
	}
	if data.AccountTo == "" {
		return errors.New("to (account id to the amount is sending) field is required")
	}
	if data.Amount < 0.1 {
		return errors.New("amount must be greater than 0.1")
	}
	return nil
}

func (data *Transaction) SaveTransaction(db *gorm.DB) (*Transaction, error) {
	var err error
	err = db.Debug().Model(&Transaction{}).Create(&data).Error
	if err != nil {
		return &Transaction{}, err
	}
	return data, nil
}

func (data *Transaction) FindTransactionsByAccountID(db *gorm.DB, accountID string) (*[]Transaction, error) {
	var err error
	transactions := []Transaction{}
	err = db.Debug().Model(&Transaction{}).Where("account_from = ?", accountID).Or("account_to = ?", accountID).Find(&transactions).Error
	if err != nil {
		return &[]Transaction{}, err
	}
	return &transactions, nil
}