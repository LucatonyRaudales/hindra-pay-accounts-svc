package models

import (
	"errors"
	"html"
	"strings"
	"time"

  "github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)
type Currency struct {
	ID        string    `gorm:"type:uuid;primary_key" json:"id"`
	Name   		string 		`gorm:"size:255;not null;unique" json:"name"`
	Code   		string 		`gorm:"size:255;not null;unique" json:"code"`
	Icon			string 		`gorm:"size:255;not null" json:"icon"`
	Enabled 	bool 			`gorm:"default:true" json:"enabled"`
	IsFiat 		bool 			`gorm:"default:false" json:"isFiat"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (wa *Currency) Prepare() {
	wa.ID = uuid.NewV4().String()
	wa.Name = html.EscapeString(strings.TrimSpace(wa.Name))
	wa.Code = html.EscapeString(strings.TrimSpace(wa.Code))
	wa.Enabled = true
	wa.CreatedAt = time.Now()
	wa.UpdatedAt = time.Now()
}

func (p *Currency) Validate() error {
	if p.Name == "" {
		return errors.New("Required wallet Name")
	}
	if p.Code == "" {
		return errors.New("Required coin")
	}
	return nil
}

func (wa *Currency) SaveCurrency(db * gorm.DB) (*Currency, error) {
	var err error
	err = db.Debug().Model(&Currency{}).Create(&wa).Error
	if err != nil {
		return &Currency{}, err
	}
	return wa, nil
}

func (p *Currency) FindAllCurrencies(db *gorm.DB) (*[]Currency, error) {
	var err error
	currenciess := []Currency{}
	err = db.Debug().Model(&Currency{}).Limit(100).Find(&currenciess).Error
	if err != nil {
		return &[]Currency{}, err
	}
	return &currenciess, nil
}

func (p *Currency) FindCurrencyByID(db *gorm.DB, pid uint64) (*Currency, error) {
	var err error
	err = db.Debug().Model(&Currency{}).Where("id = ?", pid).Take(&p).Error
	if err != nil {
		return &Currency{}, err
	}
	return p, nil
}

func (p *Currency) UpdateCurrency(db *gorm.DB) (*Currency, error) {

	var err error

	err = db.Debug().Model(&Currency{}).Where("id = ?", p.ID).Updates(Currency{Name: p.Name, Code: p.Code, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Currency{}, err
	}
	if p.ID != "" {
		err = db.Debug().Model(&Currency{}).Where("id = ?", p.ID).Take(&p.ID).Error
		if err != nil {
			return &Currency{}, err
		}
	}
	return p, nil
}

func (p *Currency) DisableCurrency(db *gorm.DB, pid uint64, ) (int64, error) {

	db = db.Debug().Model(&Currency{}).Where("id = ?", pid).Updates(Currency{Enabled: false})

	if db.Error != nil {
		/*if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Currency not found")
		}*/
		return 0, db.Error
	}
	return db.RowsAffected, nil
}