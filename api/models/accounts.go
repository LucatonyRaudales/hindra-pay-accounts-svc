package models

import (
	"time"
	"strings"
	"html"
	"errors"
  "github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

type Account struct {
	ID string `gorm:"type:uuid;primary_key" json:"id"`
	UserId string `gorm:"size:255;not null" json:"userId"`
	CurrencyID string `gorm:"index;not null" json:"currencyId"`
	Currency Currency `gorm:"foreignKey:CurrencyID" json:"currency"`
	Balance float64 `gorm:"type:decimal(10,2);not null" json:"balance"`
	Enabled bool `gorm:"default:true" json:"enabled"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"createdAt"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updatedAt"`
}

func (data *Account) Prepare() {
	data.ID = uuid.NewV4().String()
	data.UserId = html.EscapeString(strings.TrimSpace(data.UserId))
	data.CurrencyID = html.EscapeString(strings.TrimSpace(data.CurrencyID))
	data.Balance = 0.00
	data.Enabled = true
	data.CreatedAt = time.Now()
	data.UpdatedAt = time.Now()
}

func (data *Account) Validate() error { 
	if data.CurrencyID == "" {
		return errors.New("account ID is required")
	}
	if data.UserId == "" {
		return errors.New("User ID is required")
	}
	return nil
}

func (data *Account) SaveAccount(db* gorm.DB) (*Account, error) {
	var err error
	err = db.Debug().Model(&Account{}).Create(&data).Error
	if err != nil {
		return &Account{}, err
	}
	return data, nil
}

func (data *Account) FindAccountsByUserID(db *gorm.DB, userID string)(*[]Account, error) {
	var err error
	accounts := []Account{}
	err = db.Debug().Model(&Account{}).Where("user_id =?", userID).Limit(100).Preload("Currency").Find(&accounts).Error
	if err != nil {
		return &[]Account{}, err
	}
	return &accounts, nil
}

func (data *Account) GetAccountDetails(db *gorm.DB, id string) (*Account, error){
	var account Account
  err := db.Debug().Where("id =?", id).Preload("Currency").First(&account).Error
  if err != nil {
    return &Account{}, err
  }
  return &account, nil
}

func (account *Account) DebitAccountBalance(db *gorm.DB, amount float64) (bool, *Account, error) {
	var newAmount = account.Balance - amount
	if newAmount < 0 {
		return false, &Account{}, nil
	}

	err := db.Debug().Model(&Account{}).Where("id = ?",  account.ID).Updates(Account{Balance:newAmount}).Error
	if err != nil {
		return true, &Account{}, err
	}

	account.Balance = newAmount
	return true, account, nil 
}

func (account *Account) AccreditAccountBalance(db *gorm.DB, amount float64) (*Account, error) {
	var newAmount = account.Balance + amount
	err := db.Debug().Model(&Account{}).Where("id = ?", account.ID).Updates(Account{Balance:newAmount}).Error
	if err != nil {
		return &Account{}, err
	}

	account.Balance = newAmount
	return account, nil 
}

func (data *Account) GetToAndFromTransactionAccounts(db *gorm.DB, fromId, toId string) (*Account, *Account, error) {
	var err error
  fromAccount := Account{}
  err = db.Debug().Model(&Account{}).Where("id =?", fromId).Preload("Currency").First(&fromAccount).Error
  if err!= nil {
    return &Account{}, &Account{}, err
  }
	toAccount := Account{}
	err = db.Debug().Model(&Account{}).Where("id =?", toId).Preload("Currency").First(&toAccount).Error
	if err!= nil {
    return &Account{}, &Account{}, err
	}
	return &fromAccount, &toAccount, nil

}