package models

import (
	"time"
	"strings"
	"html"
	"errors"
  "github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)


type Recharge struct {
	ID string `gorm:"type:uuid;primary_key" json:"id"`
	Account string `gorm:"type:uuid;not null;" json:"account"`
	//AccountFrom Account `gorm:"foreignKey:From" json:"accountFrom"`
	//AccountTo Account `gorm:"foreignKey:To" json:"accountTo"`
	Amount float64 `gorm:"type:decimal(10,2);not null;" json:"amount"`
  PaymentMethod string `gorm:"size:128;not null;" json:"payment_method"`
  Reference string `gorm:"size:128;not null;" json:"reference"`
	Status string `gorm:"size:128" json:"status"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"createdAt"`
}
//En este ejemplo, he agregado el campo "TransactionType" para indicar si la transacción es una recarga o una transferencia, he cambiado los nombres de los campos "AccountFrom" y "AccountTo" a "Source" y "Destination" respectivamente, agregado el campo "PaymentMethod" para indicar el medio de pago utilizado, el campo "Reference" para guardar la referencia de la transacción y se mantiene el campo "Status" para indicar si la transacción fue exitosa o no.



func (data *Recharge) Prepare() {
	data.ID = uuid.NewV4().String()
	data.Account = html.EscapeString(strings.TrimSpace(data.Account))
	data.CreatedAt = time.Now()
	data.Status =  html.EscapeString(strings.TrimSpace("pending"))
	data.PaymentMethod =  html.EscapeString(strings.TrimSpace("debitTarget"))
	data.Reference =  html.EscapeString(strings.TrimSpace("reference"))
}

func (data *Recharge) Validate() error { 
	if data.Account == "" {
		return errors.New("account field is required")
	}
	if data.Amount < 0.1 {
		return errors.New("amount must be greater than 0.1")
	}
	return nil
}

func (data *Recharge) SaveRecharge(db *gorm.DB) (*Recharge, error) {
	var err error
	err = db.Debug().Model(&Recharge{}).Create(&data).Error
	if err != nil {
		return &Recharge{}, err
	}
	return data, nil
}
