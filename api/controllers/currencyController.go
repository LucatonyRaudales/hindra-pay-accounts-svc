package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	//"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/auth"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/models"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/responses"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/utils/formaterror"
)

func (server *Server) CreateCurrency(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	currency := models.Currency{}
	err = json.Unmarshal(body, &currency)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	currency.Prepare()
	err = currency.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	//uid, err := auth.ExtractTokenID(r)
	//if err != nil {
	//	responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
	//	return
	//}

	currencyCreated, err := currency.SaveCurrency(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	w.Header().Set("Lacation", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, currencyCreated.ID))
	responses.JSON(w, http.StatusCreated, currencyCreated)
}

func (server *Server) GetCurrencyList(w http.ResponseWriter, r *http.Request) {

	currency := models.Currency{}

	currencyList, err := currency.FindAllCurrencies(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, currencyList)
}

func (server *Server) GetCurrency(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	pid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	currency := models.Currency{}

	currencyResponse, err := currency.FindCurrencyByID(server.DB, pid)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, currencyResponse)
}

func (server *Server) UpdateCurrency(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	pid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	//uid, err := auth.ExtractTokenID(r)
	//if err != nil {
	//	responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
	//	return
	//}

	// Check if the Currency exist
	currency := models.Currency{}
	err = server.DB.Debug().Model(models.Currency{}).Where("id = ?", pid).Take(&currency).Error
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("Currency not found"))
		return
	}

	// Read the data Currencyed
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Start processing the request data
	currencyUpdate := models.Currency{}
	err = json.Unmarshal(body, &currencyUpdate)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	//Also check if the request user id is equal to the one gotten from token
	//if uid != CurrencyUpdate.UserID {
	//	responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
	//	return
	//}

	currencyUpdate.Prepare()
	err = currencyUpdate.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	currencyUpdate.ID = currency.ID //this is important to tell the model the Currency id to update, the other update field are set above

	CurrencyUpdated, err := currencyUpdate.UpdateCurrency(server.DB)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	responses.JSON(w, http.StatusOK, CurrencyUpdated)
}

func (server *Server) DeleteCurrency(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	// Is a valid Currency id given to us?
	pid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Is this user authenticated?
	//uid, err := auth.ExtractTokenID(r)
	//if err != nil {
	//	responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
	//	return
	//}

	// Check if the Currency exist
	Currency := models.Currency{}
	err = server.DB.Debug().Model(models.Currency{}).Where("id = ?", pid).Take(&Currency).Error
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("Unauthorized"))
		return
	}

	// Is the authenticated user, the owner of this Currency?
	//if uid != Currency.UserID {
	//	responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
	//	return
	//}
	_, err = Currency.DisableCurrency(server.DB, pid)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	w.Header().Set("Entity", fmt.Sprintf("%d", pid))
	responses.JSON(w, http.StatusNoContent, "")
}