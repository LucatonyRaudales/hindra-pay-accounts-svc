package controllers


import (
	"net/http"
	"log"
	"fmt"
	"github.com/gorilla/websocket"
)

// https://www.youtube.com/watch?v=HguckSSr-EM

var conns = map[string]*websocket.Conn{}

type NotifyUserData struct {
	Status 		string `json:"status"`
	UserName string `json:"userName"`
	FromID	string `json:"fromID"`
	Amount float64 `json:"amount"`
	TransactionID string `json:"transactionId"`
	To   		string `json:"to"`
  Description string `json:"description"`
}

func (server *Server) NotifyReceiverUser(w http.ResponseWriter, r *http.Request, data *NotifyUserData){
	conn, _, err := websocket.DefaultDialer.Dial("wss://hindra-accounts.onrender.com/transaction/sendMessage", nil)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	
			log.Println("to")
			
			log.Println(data.To)

	if con, ok := conns[data.To]; ok {
			log.Println("ok")
			log.Println(ok)
		err = con.WriteJSON(&data)
		if err != nil {
			log.Println(err)
		}
	}
}

type Request struct {
	From 		string `json:"from"`
	To   		string `json:"to"`
  Message string `json:"message"`
}

func (server *Server) ListenForTransaction(w http.ResponseWriter, r *http.Request){
	
	var reqFrom string
	h := http.Header{}
	for _, sub := range websocket.Subprotocols(r){
		h.Set("Sec-Websocket-Protocol", sub)
		fmt.Println(sub)
		reqFrom = sub
	}

	ws, err := server.Upgrader.Upgrade(w, r, h)
	if err!= nil {
    log.Println(err)
		return
  }
		conns[reqFrom] = ws

			//TODO: I should to defer the connection after used
}