package controllers
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	//"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/auth"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/models"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/responses"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/utils/formaterror"
)

func (server *Server) CreateAccount(w http.ResponseWriter, r *http.Request){
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return 
	}
	userAccount := models.Account{}
	err = json.Unmarshal(body, &userAccount)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return 
	}
	userAccount.Prepare()
	err = userAccount.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	accountCreated, err := userAccount.SaveAccount(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	w.Header().Set("Lacation", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, accountCreated.ID))
	responses.JSON(w, http.StatusCreated, accountCreated)
}

func (server *Server) GetUserAccounts(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	userAccount := models.Account{}
	userAccounts, err := userAccount.FindAccountsByUserID(server.DB,  vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, userAccounts)
}

func (server *Server) GetAccount(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
  userAccount := models.Account{}
  userAccounts, err := userAccount.GetAccountDetails(server.DB,  vars["id"])
  if err	!= nil {
	responses.ERROR(w, http.StatusInternalServerError, err)
	return
	}
	responses.JSON(w, http.StatusOK, userAccounts)
}