package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	//"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/auth"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/models"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/responses"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/utils/formaterror"
)

func (server *Server) RechargeAccount(w http.ResponseWriter, r *http.Request){
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return 
	}
	
	recharge := models.Recharge{}
	err = json.Unmarshal(body, &recharge)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return 
	}
	recharge.Prepare()
	err = recharge.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	account := models.Account{ID: recharge.Account}
	accountData, errGettingAccount := account.GetAccountDetails(server.DB, recharge.Account)
	if errGettingAccount != nil {
		responses.ERROR(w, http.StatusInternalServerError, errGettingAccount)
		return
	}
	fmt.Println(accountData)
	if !accountData.Currency.IsFiat {
		responses.ERROR(w, http.StatusInternalServerError, fmt.Errorf("Account is not Lempira or Fiat"))
		return
	}
	_, errorAccrediting := accountData.AccreditAccountBalance(server.DB, recharge.Amount)
	if errorAccrediting != nil {
		responses.ERROR(w, http.StatusInternalServerError, errorAccrediting)
		return
	}

	rechargeDone, err := recharge.SaveRecharge(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}

	w.Header().Set("Lacation", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, rechargeDone.ID))
	responses.JSON(w, http.StatusCreated, rechargeDone)
}