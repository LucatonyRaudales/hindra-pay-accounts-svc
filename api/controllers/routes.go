package controllers

import "github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/middlewares"

func (s *Server) initializeRoutes() {

	// Home Route
	s.Router.HandleFunc("/", middlewares.SetMiddlewareJSON(s.Home)).Methods("GET")

	// Login Route
	//Accounts routes
	s.Router.HandleFunc("/account/currency", middlewares.SetMiddlewareJSON(s.CreateCurrency)).Methods("POST")
	s.Router.HandleFunc("/account/currency", middlewares.SetMiddlewareJSON(s.GetCurrencyList)).Methods("GET")
	s.Router.HandleFunc("/account/currency/{id}", middlewares.SetMiddlewareJSON(s.GetCurrency)).Methods("GET")
	s.Router.HandleFunc("/account/currency/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateCurrency))).Methods("PUT")
	s.Router.HandleFunc("/account/currency/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteCurrency)).Methods("DELETE")
	s.Router.HandleFunc("/account/getAccountDetails/{id}", middlewares.SetMiddlewareJSON(s.GetAccount)).Methods("GET")
	s.Router.HandleFunc("/account/userAccount", middlewares.SetMiddlewareJSON(s.CreateAccount)).Methods("POST")
	s.Router.HandleFunc("/account/getUserAccounts/{id}", middlewares.SetMiddlewareJSON(s.GetUserAccounts)).Methods("GET")
	s.Router.HandleFunc("/transaction/sendMoney", middlewares.SetMiddlewareJSON(s.SendMoney)).Methods("POST")
	s.Router.HandleFunc("/account/transactions/{account_id}", middlewares.SetMiddlewareJSON(s.getAccountTransactions)).Methods("GET")
	s.Router.HandleFunc("/account/recharge", middlewares.SetMiddlewareJSON(s.RechargeAccount)).Methods("POST")

	s.Router.HandleFunc("/transaction/sendMessage", middlewares.SetMiddlewareJSON(s.ListenForTransaction)).Methods("GET")
	s.Router.HandleFunc("/exchange/swap/quote", middlewares.SetMiddlewareJSON(s.exchangeSwapQuote)).Methods("GET")
	s.Router.HandleFunc("/exchange/swap", middlewares.SetMiddlewareJSON(s.ExchangeBetweenAccounts)).Methods("POST")
}