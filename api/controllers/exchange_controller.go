package controllers 
import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/responses"
)

type QuoteResponse struct {
    Data struct {
        Symbol string `json:"symbol"`
        Name string `json:"name"`
        Amount float64 `json:"amount"`
        Quote map[string]struct {
            Price float64 `json:"price"`
            LastUpdated string `json:"last_updated"`
        } `json:"quote"`
    } `json:"data"`
}

func (server *Server) exchangeSwapQuote(w http.ResponseWriter, r *http.Request){
    client := &http.Client{}
    from := r.URL.Query().Get("from")
    to := r.URL.Query().Get("to")
		amount := r.URL.Query().Get("amount")
    req, err := http.NewRequest("GET", "https://pro-api.coinmarketcap.com/v1/tools/price-conversion", nil)
    if err != nil {
				responses.ERROR(w, http.StatusInternalServerError, err)
				return
    }
    req.Header.Add("X-CMC_PRO_API_KEY", os.Getenv("CMC_KEY"))
    q := req.URL.Query()
    q.Add("amount", amount)
    q.Add("symbol", from)
    q.Add("convert", to)
    req.URL.RawQuery = q.Encode()
    resp, err := client.Do(req)
    if err != nil {
				responses.ERROR(w, http.StatusInternalServerError, err)
				return
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
				responses.ERROR(w, http.StatusUnprocessableEntity, err)
				return
    }
    var quote QuoteResponse
    err = json.Unmarshal(body, &quote)
    if err != nil {
				responses.ERROR(w, http.StatusUnprocessableEntity, err)
    }
	responses.JSON(w, http.StatusOK, quote.Data)
}

func exchangeSwapQuote(from, to, amount string)(*QuoteResponse, error){
    client := &http.Client{}
    req, err := http.NewRequest("GET", "https://pro-api.coinmarketcap.com/v1/tools/price-conversion", nil)
    if err != nil {
				return &QuoteResponse{}, err
    }
    req.Header.Add("X-CMC_PRO_API_KEY", os.Getenv("CMC_KEY"))
    q := req.URL.Query()
    q.Add("amount", amount)
    q.Add("symbol", from)
    q.Add("convert", to)
    req.URL.RawQuery = q.Encode()
    resp, err := client.Do(req)
    if err != nil {
				return &QuoteResponse{}, nil
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
				return &QuoteResponse{}, err
    }
    var quote QuoteResponse
    err = json.Unmarshal(body, &quote)
    if err != nil {
				return &QuoteResponse{}, err
    }
	return &quote, nil
}

