package controllers 
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	//"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/auth"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/models"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/responses"
	"github.com/LucatonyRaudales/hindra-pay-accounts-svc/api/utils/formaterror"
)

func (server *Server) SendMoney(w http.ResponseWriter, r *http.Request) {
	body, err :=ioutil.ReadAll(r.Body)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	transaction := models.Transaction{}
	err = json.Unmarshal(body, &transaction)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	transaction.Prepare()
	err = transaction.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	account := models.Account{}
	fromAccount, toAccount, err := account.GetToAndFromTransactionAccounts(server.DB, transaction.AccountFrom, transaction.AccountTo) //
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
    return
	}

	if fromAccount.CurrencyID != toAccount.CurrencyID  {
				responses.ERROR(w, http.StatusPreconditionFailed, fmt.Errorf("Both account should be of the same currency"))
    return
	}

	haveSufficientBalance, newAccountSender,  errDebiting := fromAccount.DebitAccountBalance(server.DB, transaction.Amount)
	if errDebiting != nil {
		responses.ERROR(w, http.StatusInternalServerError, errDebiting)
		return
	}

	if !haveSufficientBalance{
		response := models.TransactionFailedResponse{Message: "Balance insufficient", Description:"You have insufficient balance to process this transaction"}
		responses.JSON(w, http.StatusPaymentRequired,	response)
		return
	}

	_, errorAccrediting := toAccount.AccreditAccountBalance(server.DB, transaction.Amount)
	if errorAccrediting != nil {
		responses.ERROR(w, http.StatusInternalServerError, errorAccrediting)
		return
	}

	transactionCreated, err := transaction.SaveTransaction(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	response := models.TransactionSuccessResponse{Message: "transaction created successfully", TransactionID:transactionCreated.ID, NewBalance: newAccountSender.Balance}
	w.Header().Set("Lacation", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, transactionCreated.ID))
	
	description :=  "this is a description of the transaction, the payer should enter the description"
	server.NotifyReceiverUser(w, r,&NotifyUserData{Status: "success", UserName: "bnito Kmla", FromID: transaction.AccountFrom, Amount: transactionCreated.Amount, TransactionID: transactionCreated.ID, To: transaction.AccountTo, Description: description})
	responses.JSON(w, http.StatusCreated, response)
}

func (server *Server) getAccountTransactions(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	transaction := models.Transaction{}
	transactions, err := transaction.FindTransactionsByAccountID(server.DB, vars["account_id"])
	if err != nil {
	responses.ERROR(w, http.StatusInternalServerError, err)
	return
	}
	responses.JSON(w, http.StatusOK, transactions)
}


func (server *Server) ExchangeBetweenAccounts(w http.ResponseWriter, r *http.Request){
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	transaction := models.Transaction{}
	err = json.Unmarshal(body, &transaction)
  if err!= nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	transaction.Prepare()
  err = transaction.Validate()
	if err!= nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
				return
	}

	account := models.Account{}
	fromAccount, toAccount, err := account.GetToAndFromTransactionAccounts(server.DB, transaction.AccountFrom, transaction.AccountTo)
	if err!= nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
    return
	}

	haveSufficientBalance, newAccountSender,  errDebiting := fromAccount.DebitAccountBalance(server.DB, transaction.Amount)
	if errDebiting != nil {
		responses.ERROR(w, http.StatusInternalServerError, errDebiting)
		return
	}

	if !haveSufficientBalance{
		response := models.TransactionFailedResponse{Message: "Balance insufficient", Description:"You have insufficient balance to process this transaction"}
		responses.JSON(w, http.StatusPaymentRequired,	response)
		return
	}

	quote, err := exchangeSwapQuote(fromAccount.Currency.Code, toAccount.Currency.Code,  fmt.Sprintf("%f", transaction.Amount))
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
    return
	}

	code := toAccount.Currency.Code
	_, errorAccrediting := toAccount.AccreditAccountBalance(server.DB, quote.Data.Quote[code].Price)
	if errorAccrediting!= nil {
		responses.ERROR(w, http.StatusInternalServerError, errorAccrediting)
				return
	}

	transactionCreated, err := transaction.SaveTransaction(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	
	response := models.SwapSuccessResponse{
		Message: "Swap has been done successfully",
		TransactionID:transactionCreated.ID, 
		FromName: newAccountSender.Currency.Name,
		FromNewBalance: newAccountSender.Balance,
		ToName: toAccount.Currency.Name, 
		ToNewBalance: toAccount.Balance,
	}
	w.Header().Set("Lacation", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, transactionCreated.ID))
	
	responses.JSON(w, http.StatusCreated, response)
}




