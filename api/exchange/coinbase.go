package exchange

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type createAddressResponse struct {
	Data struct {
		Address string `json:"address"`
	} `json:"data"`
}

func createAddress(apikey, apisecret string) (string, error) {
	req, err := http.NewRequest("POST",
	"https://api.coinbase.com/v2/accounts/primary/addresses", nil)
	if err != nil {
		return "", err
	}
	req.SetBasicAuth(apikey, apisecret)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	
	var response createAddressResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return "", err
	}

	return response.Data.Address, nil

}